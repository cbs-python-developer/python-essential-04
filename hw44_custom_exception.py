"""Homework 4 task 4 implementation."""


class TakeFromEmptyBoxError(Exception):
    pass


class AddToFullBoxError(Exception):
    pass


class Box:
    """A class to represent a box for storing items."""

    def __init__(self, capacity: int) -> None:
        self.capacity = capacity
        self._items = []

    @property
    def capacity(self) -> int:
        """The maximum number of items that can fit inside the box."""
        return self._capacity

    @capacity.setter
    def capacity(self, value):
        self._capacity = value

    @property
    def items(self) -> list:
        """List of the items in the box."""
        return self._items

    def add_item(self, item: str) -> None:
        """Add an item to the box."""
        if self.is_full():
            raise AddToFullBoxError("The box is already full")
        else:
            self._items.append(item)

    def take_item(self) -> str:
        """Take an item from the box."""
        if self.is_empty():
            raise TakeFromEmptyBoxError("No items in the box")
        else:
            return self._items.pop()

    def is_full(self) -> bool:
        """There is no room for items in the box."""
        return self.capacity == len(self.items)

    def is_empty(self) -> bool:
        """There are no items in the box."""
        return len(self.items) == 0

    def __str__(self) -> str:
        return (f"The Box of {self.capacity} unit(s) capacity "
                f"contains {self.items}")


if __name__ == '__main__':

    box1 = Box(3)
    print(box1)

    print("\n----Trying to take something out of the box")
    try:
        item1 = box1.take_item()
    except TakeFromEmptyBoxError as err:
        print(f"{err.__class__.__name__}: {err}")

    print("\n----Loading the box")
    for thing in ['book', 'ball', 'boots']:
        box1.add_item(thing)
    print(box1)

    print("\n----Adding one more item")
    try:
        box1.add_item('onion')
    except AddToFullBoxError as err:
        print(f"{err.__class__.__name__}: {err}")

    print("\n----Trying to take something out of the box")
    item1 = box1.take_item()
    print(box1)
    print(f"We got '{item1}'")
