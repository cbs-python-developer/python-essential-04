"""Simple Calculator.

Operations:
----------
+  add
-  subtract
*  multiply
/  divide
** power

Numbers
-------
floats (example: 0.42)
"""
import operator


OPERATIONS = {
    '+': operator.add,
    '-': operator.sub,
    '*': operator.mul,
    '/': operator.truediv,
    '**': operator.pow
}


def get_number(prompt: str) -> float:
    """Get a number from input."""
    while True:
        try:
            reply = float(input(f"{prompt}: "))
            return reply
        except ValueError:
            print("Invalid input!\n")


def menu():
    """Display the menu and perform the operations."""
    print("Happy Calculation!\n")
    while True:
        number1 = get_number("Enter 1st number")
        while True:
            operation_type = input(
                "Choose an operation:\n"
                "'+'  add\n"
                "'-'  subtract\n"
                "'*'  multiply\n"
                "'/'  divide\n"
                "'**' power\n"
                ": "
            )
            if operation_type in OPERATIONS:
                break
            else:
                print("Invalid operation!\n")
        number2 = get_number("Enter 2nd number")

        try:
            result = OPERATIONS[operation_type](number1, number2)
            print(f"{number1} {operation_type} {number2} = {result}")
        except ZeroDivisionError as err:
            print(f"Error: {err}")

        if input(
            "\nPress 'Enter' to continue calculation, 'Q' to Exit: "
        ).lower() == 'q':
            break
        else:
            print()


if __name__ == '__main__':
    menu()
