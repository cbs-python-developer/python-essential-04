"""Homework 4 task 3 implementation."""
from datetime import datetime
from typing import Any


DEPARTMENTS = [
    'Development',
    'Maintenance',
    'Accounting',
    'Human Resources',
]


class Employee:
    """A class to represent an employee."""

    def __init__(
            self,
            first_name: str,
            last_name: str,
            department: str,
            start_year: int
    ) -> None:
        self.first_name = first_name
        self.last_name = last_name
        self.department = department
        self.start_year = start_year

    @property
    def first_name(self) -> str:
        """The first name of an employee."""
        return self._first_name

    @first_name.setter
    def first_name(self, value: str) -> None:
        self._first_name = value

    @property
    def last_name(self) -> str:
        """The last name of an employee."""
        return self._last_name

    @last_name.setter
    def last_name(self, value: str) -> None:
        self._last_name = value

    @property
    def department(self) -> str:
        """Employee's department."""
        return self._department

    @department.setter
    def department(self, value: str) -> None:
        self._department = value

    @property
    def start_year(self) -> int:
        """The year in which an employee started work."""
        return self._start_year

    @start_year.setter
    def start_year(self, value: int) -> None:
        this_year = datetime.now().year
        if value > this_year:
            raise ValueError(
                f"The year can not be from the future: {value}"
            )
        elif value < 2001:
            raise ValueError(
                "The employee could not start work before "
                "the foundation of the company in 2001"
            )
        else:
            self._start_year = value

    def __str__(self) -> str:
        return (f"{self.first_name} {self.last_name} "
                f"from the {self.department} department "
                f"has been working since {self.start_year}")


def get_number(prompt: str) -> int:
    """Get a number from input."""
    while True:
        try:
            number = int(input(f"{prompt}: "))
            return number
        except ValueError as err:
            print(f"!Invalid input: {err}")


def choose_from_list(items: list, prompt: str) -> Any:
    """Choose an item from the list by its index."""
    items_str = '\n\t'.join([
        f"'{idx}' - {item}" for idx, item in enumerate(items)
    ])
    while True:
        item_idx = get_number(
            f"{prompt}:\n\t{items_str}\n"
        )
        try:
            return items[item_idx]
        except IndexError as err:
            print(f"!Invalid input: {err}")


def create_employee() -> Employee:
    """Create a new employee with data from the input."""
    print("\nAdding a new employee:")
    first_name = input("Enter employee's first name: ")
    last_name = input("Enter employee's last name: ")
    department = choose_from_list(DEPARTMENTS, "Choose department")
    start_year = get_number(
        "Enter the year in which the employee started work"
    )
    return Employee(first_name, last_name, department, start_year)


def add_employee() -> list[Employee]:
    staff = []
    while True:
        try:
            new_employee = create_employee()
            staff.append(new_employee)
            print("\nA new employee was added successfully!")
            print(new_employee)
        except ValueError as err:
            print(f"!Unsuccessful: {err}")
        if input(
            "\nPress 'Enter' to add an employee, 'Q' to Exit: "
        ).lower() == 'q':
            break
    return staff


def main():
    """Display the main menu and perform the actions."""
    staff = []

    while True:
        actions = [
            "Quit",
            "Add a new employee",
            "Show all staff",
            "Show the employees started from <...>",
        ]
        command = choose_from_list(actions, "\nMAIN MENU")

        match command:
            case 'Quit':
                print("Bye!")
                break
            case 'Add a new employee':
                employees = add_employee()
                if employees:
                    staff.extend(employees)
            case 'Show all staff':
                print("\nList of employees:")
                for idx, employee in enumerate(staff):
                    print(f" {idx+1}. {employee}")
            case 'Show the employees started from <...>':
                year = get_number("Enter the year")
                filtered = [
                    employee for employee in staff
                    if employee.start_year >= year
                ]
                print("\nList of employees:")
                for idx, employee in enumerate(filtered):
                    print(f" {idx+1}. {employee}")


if __name__ == '__main__':
    main()
