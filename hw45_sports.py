"""Homework 4 task 5 implementation."""
from typing import Any, Iterable

MENU = [
    'Вихід з меню',
    'Перелік видів спорту',
    'Команда тренерів',
    'Розклад тренувань',
    'Вартість тренування',
    'Пошук тренера',
]
SPORT_TYPES = {
    'Футбол': {
        'price': 500,
        'schedule': [
            ('Понеділок', '11:00'),
            ('Понеділок', '15:00'),
            ('Четвер', '11:00'),
            ('Четвер', '15:00')
        ],
        'coach': [
            'Корнерчук Михайло',
            'Офсайденко Василь',
            'Перекладенко Гриць',
        ]
    },
    'Плавання': {
        'price': 600,
        'schedule': [
            ('Вівторок', '11:00'),
            ('Вівторок', '15:00'),
            ("П'ятниця", '11:00'),
            ("П'ятниця", '15:00')
        ],
        'coach': [
            'Лящ Степан',
            'Гарпун Олег',
        ]
    },
    'Шахи': {
        'price': 450,
        'schedule': [
            ('Середа', '16:00'),
            ('Субота', '16:00'),
        ],
        'coach': [
            'Пішачук Магнус',
            'Шахов Емануель',
        ]
    },
    'Бокс': {
        'price': 400,
        'schedule': [
            ('Понеділок', '19:00'),
            ('Вівторок', '19:00'),
            ("Середа", '19:00'),
            ("П'ятниця", '19:00')
        ],
        'coach': [
            'Хук Матвій',
            'Перебитюк Дмитро',
        ]
    }
}


class NoMatchedResults(Exception):
    pass


def get_number(prompt: str) -> int:
    """Get a number from input."""
    while True:
        try:
            number = int(input(f"{prompt}: "))
            return number
        except ValueError as err:
            print(f"!Invalid input: {err}")


def choose_from_list(items: list, prompt: str) -> Any:
    """Choose an item from the list by its index."""
    items_str = '\n\t'.join([
        f"'{idx}' - {item}" for idx, item in enumerate(items)
    ])
    while True:
        item_idx = get_number(
            f"{prompt}:\n\t{items_str}\n"
        )
        try:
            return items[item_idx]
        except IndexError as err:
            print(f"!Invalid input: {err}")


def print_list(items: Iterable, title: str, numbered: bool = True) -> None:
    """Print numbered or bullet list to console."""
    print(f"\n{title}:")
    if numbered:
        for idx, item in enumerate(items):
            print(f" {idx+1}. {item}")
    else:
        for item in items:
            print(f" - {item}")


def look_for_coach() -> list:
    """Look for the coach. The prompt contains beginning of the name."""
    prompt = input("\nВведіть прізвище: ")
    coaches_matched = [
        f"{coach} - {sport}"
        for sport in SPORT_TYPES
        for coach in SPORT_TYPES[sport]['coach']
        if coach.lower().startswith(prompt.lower())
    ]
    if coaches_matched:
        return coaches_matched
    else:
        raise NoMatchedResults


def main():
    """Display the main menu and perform the actions."""
    print("Вітаємо в нашому спорткомплексі!")

    while True:
        command = choose_from_list(MENU, "\nГОЛОВНЕ МЕНЮ")

        match command:
            case 'Вихід з меню':
                print("Бувай!")
                break
            case 'Перелік видів спорту':
                print_list(SPORT_TYPES, 'ВИДИ СПОРТУ')
            case 'Команда тренерів':
                coaches = [
                    f"{coach} - {sport}"
                    for sport in SPORT_TYPES
                    for coach in SPORT_TYPES[sport]['coach']
                ]
                print_list(coaches, 'КОМАНДА ТРЕНЕРІВ')
            case 'Розклад тренувань':
                schedule = [
                    f"{sport} | {time_slot[0]} {time_slot[1]}"
                    for sport in SPORT_TYPES
                    for time_slot in SPORT_TYPES[sport]['schedule']
                ]
                print_list(schedule, 'РОЗКЛАД ТРЕНУВАНЬ', numbered=False)
            case 'Вартість тренування':
                prices = [
                    f"{sport}: {SPORT_TYPES[sport]['price']} грн."
                    for sport in SPORT_TYPES
                ]
                print_list(prices, 'ВАРТІСТЬ ТРЕНУВАННЯ', numbered=False)
            case 'Пошук тренера':
                try:
                    print_list(look_for_coach(), 'ЗНАЙДЕНО')
                except NoMatchedResults:
                    print("За результатами пошуку нічого не знайдено")


if __name__ == '__main__':
    main()
